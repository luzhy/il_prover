open Syntax;;
open Util;;

exception Prove_Error of string ;;

let new_atom_count = ref 0 ;;

let tab_flag = ref false ;;

let choose_set rule_number swff_sets =
  match swff_sets with
    | (at, c1, c2, c3, c4 ,c5 ,c6, ai) ->
        begin
          match rule_number with
            | 0 -> at
            | 1 -> c1
            | 2 -> c2
            | 3 -> c3
            | 4 -> c4
            | 5 -> c5
            | 6 -> c6
            | 7 -> ai
            | _ -> raise (Prove_Error "Error rule_number -- choose_set")
        end
;;

let choose_all swff_sets =
  match swff_sets with
    | (at, c1, c2, c3, c4 ,c5 ,c6, ai) ->
        at @ c1 @ c2 @ c3 @ c4 @ c5 @ c6 @ ai
;;

let add_to_sets new_formula rule_number swff_sets=
  match swff_sets with
    | (at, c1, c2, c3, c4 ,c5 ,c6, ai) ->
        begin 
          match rule_number with
            | 0 -> (new_formula :: at, c1, c2, c3, c4, c5, c6, ai)
            | 1 -> (at, new_formula :: c1, c2, c3, c4, c5, c6, ai)
            | 2 -> (at, c1, new_formula :: c2, c3, c4, c5, c6, ai)
            | 3 -> (at, c1, c2, new_formula :: c3, c4, c5, c6, ai)
            | 4 -> (at, c1, c2, c3, new_formula :: c4, c5, c6, ai)
            | 5 -> (at, c1, c2, c3, c4, new_formula :: c5, c6, ai)
            | 6 -> (at, c1, c2, c3, c4, c5, new_formula :: c6, ai)
            | 7 -> (at, c1, c2, c3, c4, c5, c6, new_formula :: ai)
            | _ -> raise (Prove_Error "Error rule_number -- add_to_sets")
        end
;;

let modify_sets new_set rule_number swff_sets=
  match swff_sets with
    | (at, c1, c2, c3, c4 ,c5 ,c6, ai) ->
        begin 
          match rule_number with
            | 0 -> (new_set, c1, c2, c3, c4, c5, c6, ai)
            | 1 -> (at, new_set, c2, c3, c4, c5, c6, ai)
            | 2 -> (at, c1, new_set, c3, c4, c5, c6, ai)
            | 3 -> (at, c1, c2, new_set, c4, c5, c6, ai)
            | 4 -> (at, c1, c2, c3, new_set, c5, c6, ai)
            | 5 -> (at, c1, c2, c3, c4, new_set, c6, ai)
            | 6 -> (at, c1, c2, c3, c4, c5, new_set, ai)
            | 7 -> (at, c1, c2, c3, c4, c5, c6, new_set)
            | _ -> raise (Prove_Error "Error rule_number -- modify_sets")
        end
;;

let rec classify_set new_formula_list swff_sets =
  let classify_swff hd =
    match hd with
      | Sign_T (wff) -> 
          begin
            match wff with
              | Atom(x)      -> 
                  if (is_Exist (Sign_F(wff)) (choose_set 0 swff_sets)) ||
                     (is_Exist (Sign_Fc(wff)) (choose_set 0 swff_sets))
                  then tab_flag := true
                  else begin tab_flag := !tab_flag || false end;
                  let add_atoms = add_to_sets hd 0 swff_sets in
                  let add_ais   = modify_sets (List.append (choose_set 7 add_atoms) (choose_set 1 add_atoms)) 1 add_atoms in
                    modify_sets [] 7 add_ais

              | Not (p)      -> add_to_sets hd 1 swff_sets 
              | And (pa, pb) -> add_to_sets hd 1 swff_sets 
              | Or  (pa, pb) -> add_to_sets hd 2 swff_sets 
              | Imp (pa, pb) ->
                  begin
                    match pa with
                      | Atom(x)      -> add_to_sets hd 1 swff_sets 
                      | Not (p)      -> add_to_sets hd 4 swff_sets 
                      | And (pa, pb) -> add_to_sets hd 1 swff_sets 
                      | Or  (pa, pb) -> add_to_sets hd 1 swff_sets 
                      | Imp (pa, pb) -> add_to_sets hd 4 swff_sets 
                  end
          end
      | Sign_F (wff) ->
          begin
            match wff with
              | Atom(x)      ->
                  if (is_Exist (Sign_T(wff)) (choose_set 0 swff_sets))
                  then tab_flag := true
                  else begin tab_flag := !tab_flag || false end;
                  let add_atoms = add_to_sets hd 0 swff_sets in
                  let add_ais   = modify_sets (List.append (choose_set 7 add_atoms) (choose_set 1 add_atoms)) 1 add_atoms in
                    modify_sets [] 7 add_ais

              | Not (p)      -> add_to_sets hd 3 swff_sets 
              | And (pa, pb) -> add_to_sets hd 2 swff_sets 
              | Or  (pa, pb) -> add_to_sets hd 1 swff_sets 
              | Imp (pa, pb) -> add_to_sets hd 3 swff_sets 
          end

      | Sign_Fc(wff) ->
          begin
            match wff with
              | Atom(x)      ->
                  if (is_Exist (Sign_T(wff)) (choose_set 0 swff_sets))
                  then tab_flag := true
                  else begin tab_flag := !tab_flag || false end;
                  let add_atoms = add_to_sets hd 0 swff_sets in
                  let add_ais   = modify_sets (List.append (choose_set 7 add_atoms) (choose_set 1 add_atoms)) 1 add_atoms in
                    modify_sets [] 7 add_ais

              | Not (p)      -> add_to_sets hd 5 swff_sets 
              | And (pa, pb) -> add_to_sets hd 6 swff_sets 
              | Or  (pa, pb) -> add_to_sets hd 1 swff_sets 
              | Imp (pa, pb) -> add_to_sets hd 5 swff_sets 
          end
  in
    match new_formula_list with
      | [] -> swff_sets
      | hd :: tl ->
          let new_swff_sets = classify_swff hd in
            classify_set tl new_swff_sets
;;

let rec 
  basic_tab swff_sets = 
  (*print_sets swff_sets; print_endline "";*)
  if !tab_flag 
  then 
    begin 
      tab_flag := false; 
      (*print_endline "closed";*) 
      Real_Proof (TLeaf (choose_all swff_sets))
    end
  else
    begin
      c1_rule (choose_set 1 swff_sets) swff_sets
    end

and 
  c1_rule c1swff_set swff_sets =
  match c1swff_set with
    | []                -> 
        begin
          c2_rule (choose_set 2 swff_sets) swff_sets
        end
    | hd_swff :: tl_set ->
        let new_swff_sets = (modify_sets tl_set 1 swff_sets) in
          match hd_swff with
            | Sign_T(wff) ->
                begin
                  match wff with
                    | Not(p)      ->
                        begin
                          match basic_tab (classify_set [Sign_Fc(p)] new_swff_sets) with
                            | Real_Proof pta -> Real_Proof (TSNode ((choose_all swff_sets), pta))
                            | Null_Proof kma -> Null_Proof kma
                        end

                    | And(pa, pb) -> 
                        begin
                          match basic_tab (classify_set [Sign_T(pa); Sign_T(pb)] new_swff_sets) with
                            | Real_Proof pta -> Real_Proof (TSNode ((choose_all swff_sets), pta))
                            | Null_Proof kma -> Null_Proof kma
                        end
                    | Imp(pt, pc) -> 
                        begin
                          match pt with
                            | Atom(p)     -> 
                                if is_Exist (Sign_T(pt)) (choose_set 0 swff_sets)
                                then
                                  begin
                                    match basic_tab (classify_set [Sign_T(pt); Sign_T(pc)] new_swff_sets) with
                                      | Real_Proof pta -> Real_Proof (TSNode ((choose_all swff_sets), pta))
                                      | Null_Proof kma -> Null_Proof kma
                                  end
                                else
                                  begin 
                                    match basic_tab (add_to_sets hd_swff 7 new_swff_sets) with
                                      | Real_Proof pta -> Real_Proof (TSNode ((choose_all swff_sets), pta))
                                      | Null_Proof kma -> Null_Proof kma
                                  end

                            | And(pa, pb) ->
                                begin
                                  match basic_tab (classify_set [Sign_T(Imp(pa, Imp(pb, pc)))] new_swff_sets) with
                                    | Real_Proof pta -> Real_Proof (TSNode ((choose_all swff_sets), pta))
                                    | Null_Proof kma -> Null_Proof kma
                                end

                            | Or (pa, pb) -> 
                                let np = Atom ("PP" ^ (string_of_int !new_atom_count)) in
                                  begin
                                    new_atom_count := !new_atom_count + 1;
                                    match basic_tab (classify_set [Sign_T(Imp(pa, np)); Sign_T(Imp(pb, np)); Sign_T(Imp(np, pc))] new_swff_sets) with
                                      | Real_Proof pta -> Real_Proof (TSNode ((choose_all swff_sets), pta))
                                      | Null_Proof kma -> Null_Proof kma
                                  end

                            | _ -> raise (Prove_Error "Imp matching error -- c1_rule")
                        end
                    | _ -> raise (Prove_Error "Sign_T matching error -- c1_rule")
                end
            | Sign_F(wff) ->
                begin
                  match wff with
                    | Or(pva, pvb) ->
                        begin 
                          match basic_tab (classify_set [Sign_F(pva);Sign_F(pvb)] new_swff_sets) with
                            | Real_Proof pta -> Real_Proof (TSNode ((choose_all swff_sets), pta))
                            | Null_Proof kma -> Null_Proof kma
                        end
                    | _ -> raise (Prove_Error "Sign_F matching error -- c1_rule")
                end
            | Sign_Fc(wff) ->
                begin
                  match wff with
                    | Or(pva, pvb) ->
                        begin 
                          match basic_tab (classify_set [Sign_Fc(pva);Sign_Fc(pvb)] new_swff_sets) with
                            | Real_Proof pta -> Real_Proof (TSNode ((choose_all swff_sets), pta))
                            | Null_Proof kma -> Null_Proof kma
                        end
                    | _ -> raise (Prove_Error "Sign_Fc matching error -- c1_rule")
                end
and
  c2_rule c2swff_set swff_sets =
  match c2swff_set with
    | []                -> 
        begin 
          c3_c4_rule (choose_set 3 swff_sets) (choose_set 4 swff_sets) swff_sets
        end
    | hd_swff :: tl_set ->
        let new_swff_sets = modify_sets tl_set 2 swff_sets in
          match hd_swff with
            | Sign_T (wff) ->
                begin
                  match wff with
                    | Or (pa, pb) ->
                        let pt1 = (basic_tab (classify_set [Sign_T(pa)] new_swff_sets)) in
                        let pt2 = (basic_tab (classify_set [Sign_T(pb)] new_swff_sets)) in
                          begin
                            match (pt1, pt2) with
                              | (Real_Proof prooftree1, Real_Proof prooftree2) -> 
                                  Real_Proof (TBNode ((choose_all swff_sets), prooftree1, prooftree2))
                              | (Real_Proof prooftree1, Null_Proof kmodel1)    -> 
                                  Null_Proof kmodel1
                              | (Null_Proof kmodel1,    Real_Proof prooftree1) -> 
                                  Null_Proof kmodel1
                              | (Null_Proof kmodel1,    Null_Proof kmodel2)    -> 
                                  Null_Proof kmodel1
                          end
                    | _ -> raise (Prove_Error "Sign_T matching error -- c2_rule")
                end
            | Sign_F (wff) ->
                begin
                  match wff with
                    | And(pa, pb) ->
                        let pt1 = (basic_tab (classify_set [Sign_F(pa)] new_swff_sets)) in
                        let pt2 = (basic_tab (classify_set [Sign_F(pb)] new_swff_sets)) in
                          begin
                            match (pt1, pt2) with
                              | (Real_Proof prooftree1, Real_Proof prooftree2) -> 
                                  Real_Proof (TBNode ((choose_all swff_sets), prooftree1, prooftree2))
                              | (Real_Proof prooftree1, Null_Proof kmodel1)    -> 
                                  Null_Proof kmodel1
                              | (Null_Proof kmodel1,    Real_Proof prooftree1) -> 
                                  Null_Proof kmodel1
                              | (Null_Proof kmodel1,    Null_Proof kmodel2)    -> 
                                  Null_Proof kmodel1
                          end
                    | _ -> raise (Prove_Error "Sign_F matching error -- c2_rule")
                end
            | _ -> raise (Prove_Error "Sign matching error -- c2_rule")
and
  c3_c4_rule c3swff_set c4swff_set swff_sets =

  let c3_one_rule hd_swff swff_sets =
    let new_swff_sets = modify_sets [] 3 swff_sets in
    let new_swff_sets2 = modify_sets (fst (filter_set (choose_set 0 new_swff_sets))) 0 new_swff_sets in
      match hd_swff with
        | Sign_F(wff) ->
            begin
              match wff with
                | Not(p)      -> 
                    begin
                      match basic_tab (classify_set [Sign_T(p)] new_swff_sets2) with
                        | Real_Proof pta -> Real_Proof (TSNode ((choose_all swff_sets), pta))
                        | Null_Proof kma ->
                            if (List.length (snd (filter_set (choose_set 0 swff_sets))) = 0) &&
                               (List.length (choose_set 3 swff_sets) <= 1) 
                            then Null_Proof kma
                            else Null_Proof (TNextWorld ((choose_set 0 swff_sets), kma))
                    end
                | Imp(pa, pb) -> 
                    begin
                      match basic_tab (classify_set [Sign_T(pa); Sign_F(pb)] new_swff_sets2) with
                        | Real_Proof pta -> Real_Proof (TSNode ((choose_all swff_sets), pta))
                        | Null_Proof kma ->
                            if (List.length (snd (filter_set (choose_set 0 swff_sets))) = 0) &&
                               (List.length (choose_set 3 swff_sets) <= 1) 
                            then Null_Proof kma
                            else Null_Proof (TNextWorld ((choose_set 0 swff_sets), kma))
                    end
                | _ -> raise (Prove_Error "Sign_F matching error -- c3_rule")
            end
        | _ -> raise (Prove_Error "Sign matching error -- c3_rule")

  and c4_one_rule hd_swff new_swff_sets =
    let new_swff_sets2 = modify_sets [] 3 new_swff_sets in
    let new_swff_sets3 = modify_sets (fst (filter_set (choose_set 0 new_swff_sets2))) 0 new_swff_sets2 in
      match hd_swff with
        | Sign_T (wff) ->
            begin
              match wff with
                | Imp(pt, pc) -> 
                    begin
                      match pt with
                        | Not(p)      ->
                            let pta = (basic_tab (classify_set [Sign_T(p) ] new_swff_sets3)) in
                            let ptb = (basic_tab (classify_set [Sign_T(pc)] new_swff_sets)) in
                              begin
                                match (pta, ptb) with
                                  | (Real_Proof prooftree1, Real_Proof prooftree2) -> 
                                      Real_Proof (TBNode ((choose_all new_swff_sets), prooftree1, prooftree2))
                                  | (Real_Proof prooftree1, Null_Proof kmodel1)    -> 
                                      Null_Proof kmodel1
                                  | (Null_Proof kmodel1,    Real_Proof prooftree1) -> 
                                      if (List.length (snd (filter_set (choose_set 0 new_swff_sets)))) = 0 &&
                                         (List.length (choose_set 3 new_swff_sets)) = 0
                                      then Null_Proof kmodel1
                                      else Null_Proof (TNextWorld ((choose_set 0 new_swff_sets), kmodel1))
                                  | (Null_Proof kmodel1,    Null_Proof kmodel2)    -> 
                                      Null_Proof kmodel2
                              end

                        | Imp(pa, pb) -> 
                            let newp = Atom ("PP" ^ (string_of_int !new_atom_count)) in
                              new_atom_count := !new_atom_count + 1;
                              let pta = (basic_tab (classify_set [Sign_T(pa);
                                                                  Sign_F(newp);
                                                                  Sign_T(Imp(pb, newp));
                                                                  Sign_T(Imp(newp, pc))] new_swff_sets3)) in
                              let ptb = (basic_tab (classify_set [Sign_T(pc)] new_swff_sets)) in
                                begin
                                  match (pta, ptb) with
                                    | (Real_Proof prooftree1, Real_Proof prooftree2) -> 
                                        Real_Proof (TBNode ((choose_all new_swff_sets), prooftree1, prooftree2))
                                    | (Real_Proof prooftree1, Null_Proof kmodel1)    -> 
                                        Null_Proof kmodel1
                                    | (Null_Proof kmodel1,    Real_Proof prooftree1) -> 
                                        if (List.length (snd (filter_set (choose_set 0 new_swff_sets)))) = 0 &&
                                           (List.length (choose_set 3 new_swff_sets)) = 0
                                        then Null_Proof kmodel1
                                        else Null_Proof (TNextWorld ((choose_set 0 new_swff_sets), kmodel1))
                                    | (Null_Proof kmodel1,    Null_Proof kmodel2)    -> 
                                        Null_Proof kmodel2
                                end
                        | _ -> raise (Prove_Error "Imp matching error -- c4_rule")
                    end
                | _ -> raise (Prove_Error "Sign_T matching error -- c4_rule")
            end
        | _ -> raise (Prove_Error "Sign matching error -- c4_rule")

  and foreach_rule func3 func4 swff_sets =
    let rec iter4 setp setq branchworlds=
      match setq with
        | [] -> 
            begin
              match branchworlds with
                | TSlibWorld (kma, (TLWorld [])) -> Null_Proof kma
                | _ -> Null_Proof (TNextWorld ((choose_set 0 swff_sets), branchworlds))
            end

        | hd :: tl ->
            begin
              match (func4 hd (modify_sets (List.append setp tl) 4 swff_sets)) with
                | Real_Proof pta -> Real_Proof pta
                | Null_Proof kma -> iter4 (List.append setp [hd]) tl (TSlibWorld (kma, branchworlds))
            end
    in
    let rec iter3 setp setq branchworlds=
      match setq with
        | [] -> 
            iter4 [] (choose_set 4 swff_sets) branchworlds
        | hd :: tl ->
            begin
              match (func3 hd (modify_sets (List.append setp tl) 3 swff_sets)) with
                | Real_Proof pta -> Real_Proof pta
                | Null_Proof kma -> iter3 (List.append setp [hd]) tl (TSlibWorld (kma, branchworlds))
            end
    in
      iter3 [] (choose_set 3 swff_sets) (TLWorld [])
  in
    if (List.length c3swff_set = 0) && (List.length c4swff_set = 0)
    then c5_rule (choose_set 5 swff_sets) swff_sets
    else
      begin
        match ((List.length (snd (filter_set (choose_set 0 swff_sets)))), c3swff_set) with
          | (0, []) ->
              begin
                match c4swff_set with
                  | []              -> raise (Prove_Error "Set matching error -- c3_c4_rule")
                  | hd_swff::tl_set ->
                      begin
                        match (c4_one_rule hd_swff (modify_sets tl_set 4 swff_sets)) with
                          | Real_Proof pta -> Real_Proof pta
                          | Null_Proof kma -> Null_Proof kma
                      end
              end
          | (0, [hd_swff]) ->
              begin
                match (c3_one_rule hd_swff swff_sets) with
                  | Real_Proof pta -> Real_Proof pta
                  | Null_Proof kma -> Null_Proof kma
              end

          | _  ->
              let fc = ((foreach_rule c3_one_rule c4_one_rule swff_sets)) in
                begin
                  match fc with
                    | Real_Proof ptb -> Real_Proof ptb
                    | Null_Proof kma -> Null_Proof kma
                end
      end

and
  c5_rule c5swff_set swff_sets =
  match c5swff_set with
    | []                -> 
        begin
          c6_rule (choose_set 6 swff_sets) swff_sets
        end
    | hd_swff :: tl_set ->
        let tmp_swff_sets = modify_sets tl_set 5 swff_sets in
        let new_swff_sets = modify_sets (fst (filter_set (choose_set 0 tmp_swff_sets))) 0 tmp_swff_sets in
          match hd_swff with
            | Sign_Fc(wff) ->
                begin
                  match wff with
                    | Not(p)      -> 
                        begin
                          match basic_tab (classify_set [Sign_T(p)] new_swff_sets) with
                            | Real_Proof ptb -> Real_Proof (TSNode ((choose_all swff_sets), ptb))
                            | Null_Proof kma -> 
                                if (List.length (snd (filter_set (choose_set 0 tmp_swff_sets)))) = 0
                                then Null_Proof kma
                                else Null_Proof (TNextWorld ((choose_set 0 swff_sets), kma))
                        end

                    | Imp(pa, pb) -> 
                        begin 
                          match basic_tab (classify_set [Sign_T(pa); Sign_Fc(pb)] new_swff_sets) with
                            | Real_Proof ptb -> Real_Proof (TSNode ((choose_all swff_sets), ptb))
                            | Null_Proof kma ->
                                if (List.length (snd (filter_set (choose_set 0 tmp_swff_sets)))) = 0
                                then Null_Proof kma
                                else Null_Proof (TNextWorld ((choose_set 0 swff_sets), kma))
                        end
                    | _ -> raise (Prove_Error "Sign_Fc matching error -- c5_rule")
                end
            | _ -> raise (Prove_Error "Sign matching error -- c5_rule")
and
  c6_rule c6swff_set swff_sets =
  match c6swff_set with
    | []                -> Null_Proof (TLWorld (choose_set 0 swff_sets))
    | hd_swff :: tl_set ->
        let tmp_swff_sets = modify_sets tl_set 6 swff_sets in
        let new_swff_sets = modify_sets (fst (filter_set (choose_set 0 tmp_swff_sets))) 0 tmp_swff_sets in
          match hd_swff with
            | Sign_Fc(wff) ->
                begin
                  match wff with
                    | And(pa, pb) -> 
                        let pta = (basic_tab (classify_set [Sign_Fc(pa)] new_swff_sets)) in
                        let ptb = (basic_tab (classify_set [Sign_Fc(pb)] new_swff_sets)) in
                          begin
                            match (pta, ptb) with
                              | (Real_Proof prooftree1, Real_Proof prooftree2) ->  
                                  Real_Proof (TBNode ((choose_all swff_sets), prooftree1, prooftree2))

                              | (Real_Proof prooftree1, Null_Proof kmodel1)    -> 
                                  if (List.length (snd (filter_set (choose_set 0 tmp_swff_sets)))) = 0
                                  then Null_Proof kmodel1
                                  else Null_Proof (TNextWorld ((choose_set 0 swff_sets), kmodel1))

                              | (Null_Proof kmodel1,    Real_Proof prooftree1) -> 
                                  if (List.length (snd (filter_set (choose_set 0 tmp_swff_sets)))) = 0
                                  then Null_Proof kmodel1
                                  else Null_Proof (TNextWorld ((choose_set 0 swff_sets), kmodel1))

                              | (Null_Proof kmodel1,    Null_Proof kmodel2)    -> 
                                  if (List.length (snd (filter_set (choose_set 0 tmp_swff_sets)))) = 0
                                  then Null_Proof kmodel1
                                  else Null_Proof (TNextWorld ((choose_set 0 swff_sets), kmodel1))
                          end
                    | _ -> raise (Prove_Error "Sign_Fc matching error -- c6_rule")
                end

            | _ -> raise (Prove_Error "Sign matching error -- c6_rule")
;;

let prove swff =
  tab_flag := false;
  new_atom_count := 0;
  basic_tab (classify_set swff ([],[],[],[],[],[],[],[]))
;;

