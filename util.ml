open Syntax;;

let rec is_Exist swff set =
  match set with 
    | [] -> false
    | hd :: tl ->
        match (hd, swff) with
          | (Sign_T(Atom(x)), Sign_T(Atom(y))) -> 
              if x = y then true
              else is_Exist swff tl
          | (Sign_F(Atom(x)), Sign_F(Atom(y))) -> 
              if x = y then true
              else is_Exist swff tl
          | (Sign_Fc(Atom(x)), Sign_Fc(Atom(y))) -> 
              if x = y then true
              else is_Exist swff tl
          | _ -> 
              is_Exist swff tl
;;

let rec filter_set set =
  let rec iter tup tset =
    match tset with
      | [] -> tup
      | hd::tl ->
          match hd with
            | Sign_F(x) -> iter ((fst tup), hd::(snd tup)) tl
            | _         -> iter (hd::(fst tup), (snd tup)) tl
  in
    iter ([],[]) set
;;

let rec print_wff wff =
  match wff with
    | Atom(x)  ->
        print_string x
    | Not(x)   ->
        print_string "~";
        print_wff x
    | And(x,y) ->
        print_string "(";
        print_wff x;
        print_string " & ";
        print_wff y;
        print_string ")"
    | Or(x,y)  ->
        print_string "(";
        print_wff x;
        print_string " | ";
        print_wff y;
        print_string ")"
    | Imp(x,y) ->
        print_string "(";
        print_wff x;
        print_string " => ";
        print_wff y;
        print_string ")"
;;

let print_swff swff =
  match swff with
    | Sign_T(x)  ->
        print_string "T";
        print_string "(";
        print_wff x;
        print_string ")"
    | Sign_F(x)  ->
        print_string "F";
        print_string "(";
        print_wff x;
        print_string ")"
    | Sign_Fc(x) ->
        print_string "Fc";
        print_string "(";
        print_wff x;
        print_string ")"
;;

let print_set set =
  let rec recp set =
    match set with
      | [] -> ()
      | [hd] -> 
          print_swff hd;
          recp []
      | hd :: tl ->
          print_swff hd;
          print_string "; ";
          recp tl
  in
    print_string "[ ";
    recp set;
    print_string " ]"
;;

let print_sets sets =
  match sets with
    | (at, c1, c2, c3, c4, c5, c6, ai) ->
        print_string "{";
        print_set at;
        print_string ", ";
        print_set c1;
        print_string ", ";
        print_set c2;
        print_string ", ";
        print_set c3;
        print_string ", ";
        print_set c4;
        print_string ", ";
        print_set c5;
        print_string ", ";
        print_set c6;
        print_string ", ";
        print_set ai;
        print_string "}"
;;

let rec non_dup set =
  match set with
    | [] -> []
    | hd :: tl ->
        if is_Exist hd tl
        then non_dup tl
        else hd :: (non_dup tl)
;;

let rec unsign set =
  match set with
    | [] -> []
    | hd :: tl ->
        match hd with
          | Sign_T (x) -> x :: (unsign tl)
          | Sign_F (x) -> x :: (unsign tl)
          | Sign_Fc(x) -> x :: (unsign tl)
;;

let rec t_filter set =
  let rec iter lst tset =
    match tset with
      | [] -> lst
      | hd::tl ->
          match hd with
            | Sign_T(x) -> iter (hd::lst) tl
            | _         -> iter lst tl
  in
    iter [] set
;;

let print_wffset set =
  let rec iter set =
    match set with
      | [] -> ()
      | [hd] ->
          print_wff hd
      | hd :: tl -> 
          print_wff hd;
          print_string ", ";
          iter tl
  in
    print_string "[";
    iter set;
    print_string "]"
;;

let pp_filter set =
  let rec iter set =
    match set with
      | [] -> []
      | hd :: tl ->
          begin
          match hd with
            | Atom(x) -> 
                if String.length x > 2 &&
                   String.sub x 0 2 = "PP"
                then iter tl
                else hd :: (iter tl)
            | _       -> 
                iter tl
          end
  in
    iter set
;;

let print_worlds worlds =
  let rec iter wld blk ctr=
    match wld with
      | TLWorld(s1) -> 
          let news1 = pp_filter (unsign (non_dup (t_filter s1))) in
            print_string blk;
            print_int ctr;
            print_string " - ";
            print_wffset news1;
            print_endline "";
      | TSlibWorld(w1, w2) ->
          begin
            match w2 with
              | TLWorld [] -> 
                  iter w1 blk (ctr);
              | _ -> 
                  iter w1 blk (ctr);
                  iter w2 blk (ctr)
          end
      | TNextWorld(s1, w2) ->
          let news1 = pp_filter (unsign (non_dup (t_filter s1))) in
            begin
              print_string blk;
              print_int ctr;
              print_string " - ";
              print_wffset news1;
              print_endline "";
              iter w2 (blk ^ " ") (ctr + 1)
            end
  in
    iter worlds "  " 0
;;

let print_prooftable ptable =
  let rec iter ptb blk ctr =
    match ptb with
      | TLeaf lst ->
          print_string blk;
          print_int ctr;
          print_string " - ";
          print_set (non_dup lst);
          print_endline ""
      | TSNode (lst, ntable) ->
          print_string blk;
          print_int ctr;
          print_string " - ";
          print_set (non_dup lst);
          print_endline "";
          iter ntable (blk ^ " ") (ctr + 1)
      | TBNode (lst, nt1, nt2) ->
          print_string blk;
          print_int ctr;
          print_string " - ";
          print_set (non_dup lst);
          print_endline "";
          iter nt1 (blk ^ " ") (ctr + 1);
          iter nt2 (blk ^ " ") (ctr + 1)
  in
    iter ptable "  " 0
;;

let help_manual =
  "instructions: 
   premise 
       - input an formula as premise
   conjecture
       - input an formula as conjecture
   status
       - check all the input premises and conjecture
   prove
       - prove the conjecture with the premises
   proof
       - print the proof table if the result is provable
   kripke
       - print the counter model if the result is unprovable
   cancel
       - clear all the input and result
   file
       - read a formatted file for premises and conjecture
   help
       - show help manual
   exit
       - EXIT
"
;;
