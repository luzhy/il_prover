
open Syntax;;
open Prover;;
open Util;;

let parse_file filename =
  Iparse.main Ilex.token (Lexing.from_channel (open_in filename))
;;

let parse_str ftype str=
  Iparse.main Ilex.token (Lexing.from_string ("fof(test," ^ ftype ^ ",(" ^ str ^ "))."))
;;

let rec main_loop fset_flag fset res_flag res_tup =
  print_string "> "; flush stdout;
  try
    let order = read_line () in
      match order with
        | "premise"    ->
            begin
              print_string ">: "; flush stdout;
              try
                let formu_str = read_line () in
                let formu_dat = parse_str "axiom" formu_str in
                  main_loop 
                    fset_flag ((List.hd formu_dat) :: fset)
                    0         (TLeaf [], TLWorld [])
              with
                  End_of_file ->
                    print_endline "";
                    main_loop fset_flag fset res_flag res_tup 
            end
        | "conjecture" ->
            if fset_flag = false
            then
              begin
                try
                  print_string ">: "; flush stdout;
                  let formu_str = read_line ()    in
                  let formu_dat = parse_str "conjecture" formu_str in
                    main_loop 
                      true (fset @ formu_dat)
                      0    (TLeaf [], TLWorld [])
                with
                    End_of_file ->
                      print_endline "";
                      main_loop fset_flag fset res_flag res_tup 
              end
            else
              begin
                print_endline " - Conjecture is ready";
                main_loop fset_flag fset res_flag res_tup 
              end

        | "status"     -> 
            let rec status_iter fset count =
              begin
                match fset with
                  | []   -> 
                      print_endline " - Empty"
                  | [hd] ->
                      if fset_flag = true
                      then 
                        begin
                          print_string " - Conjecture: "; print_swff hd; print_endline ""
                        end
                      else 
                        begin
                          print_string " - Premise "; print_int count; print_string " : "; print_swff hd; print_endline ""
                        end
                  | hd::tl ->
                      begin
                        print_string " - Premise "; print_int count; print_string " : "; print_swff hd; print_endline "";
                        status_iter tl (count+1)
                      end
              end
            in
              status_iter fset 0;
              main_loop fset_flag fset res_flag res_tup 

        | "prove"      ->
            if fset_flag = false
            then
              begin
                print_endline " - Conjecture is NOT ready";
                main_loop fset_flag fset res_flag res_tup 
              end
            else
              begin
                match res_flag with
                  | 0 -> 
                      let prove_res = prove fset in
                        begin
                          match prove_res with
                            | Null_Proof nproof -> 
                                print_endline " - Unprovable";
                                main_loop fset_flag fset 2 (TLeaf [], nproof)
                            | Real_Proof rproof -> 
                                print_endline " - Provable";
                                main_loop fset_flag fset 1 (rproof,   TLWorld [])
                        end
                  | 1 -> 
                      print_endline " - Provable";
                      main_loop fset_flag fset res_flag res_tup 
                  | 2 ->
                      print_endline " - Unprovable";
                      main_loop fset_flag fset res_flag res_tup 
                  | _ -> failwith "Error: Unknown Flag -- prove"
              end

        | "proof"      ->
            let match_res res_flag =
              begin
                match res_flag with
                  | 0 -> print_string  " - formulas are NOT proved yet"
                  | 1 -> 
                      print_endline    " - Proof Table: ";
                      print_prooftable (fst res_tup)
                  | 2 -> print_string  " - formulas are NOT unprovable"
                  | _ -> failwith "Error: Unknown Flag -- proof"
              end
            in
              begin
                match_res res_flag;
                print_endline "";
                main_loop fset_flag fset res_flag res_tup 
              end

        | "kripke"     ->
            let match_res res_flag =
              begin
                match res_flag with
                  | 0 -> print_string " - formulas are NOT proved yet"
                  | 1 -> print_string " - formulas are NOT provable"
                  | 2 -> 
                      print_endline " - Counter Model: ";
                      print_worlds (snd res_tup)
                  | _ -> failwith "Error: Unknown Flag -- kripke"
              end
            in
              begin
                match_res res_flag;
                print_endline "";
                main_loop fset_flag fset res_flag res_tup 
              end

        | "cancel"     -> 
            main_loop false [] 0 (TLeaf [], TLWorld [])

        | "file"       ->
            begin
              print_string ">: "; flush stdout;
              try
                let filename = read_line () in
                  main_loop true (parse_file filename) 0 (TLeaf [], TLWorld [])
              with
                  End_of_file ->
                    print_endline "";
                    main_loop fset_flag fset res_flag res_tup 
            end

        | "help"       -> 
            print_string help_manual;
            main_loop fset_flag fset res_flag res_tup 

        | "exit"       ->
            print_endline " - EXit"

        | _            ->
            print_endline " - Error: Wrong Order";
            print_endline " - Input \"help\" to read instruction manual";
            main_loop fset_flag fset res_flag res_tup 
  with
    | Sys_error mesg ->
        print_endline mesg;
        main_loop false [] 0 (TLeaf [], TLWorld [])
    | Ilex.Lexer_Error mesg ->
        print_endline mesg;
        main_loop fset_flag fset res_flag res_tup 
    | Syntax.Parser_Error mesg ->
        print_endline mesg;
        main_loop fset_flag fset res_flag res_tup 
    | Prover.Prove_Error mesg ->
        print_endline ("Prove Error : " ^ mesg);
        main_loop false [] 0 (TLeaf [], TLWorld [])
    | End_of_file ->
        print_endline "";
        print_endline " - ExiT"
;;

let _ =
  print_endline "Input \"help\" to read instruction manual";
  main_loop false [] 0 (TLeaf [], TLWorld [])
;;

