exception Parser_Error of string;;

type t_wff =
    | Atom of string
    | Not  of t_wff
    | And  of t_wff * t_wff
    | Or   of t_wff * t_wff
    | Imp  of t_wff * t_wff
;;

type t_swff =
    | Sign_T  of t_wff
    | Sign_F  of t_wff
    | Sign_Fc of t_wff
;;

type t_prooftree =
    | TLeaf    of t_swff list
    | TSNode   of t_swff list * t_prooftree
    | TBNode   of t_swff list * t_prooftree * t_prooftree
;;

type t_world =
    | TLWorld    of t_swff list
    | TSlibWorld of t_world * t_world
    | TNextWorld of t_swff list * t_world
;;

type t_proof =
    | Null_Proof of t_world
    | Real_Proof of t_prooftree
;;

