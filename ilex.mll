{
open Iparse
exception Lexer_Error of string
}


let space = [' ' '\t' '\r'] 
let retur = '\n' 
let alpha = ['A'-'Z' 'a'-'z' '0'-'9' '_']

rule token = parse
| "~"     { NOT }
| "&"     { AND }
| "|"     { OR }
| "=>"    { IMP }
| "<=>"   { EQU }

| "."     { DOT } 
| ","     { COMMA }
| "("     { LPAREN }
| ")"     { RPAREN }

| "axiom" { AXIO }
| "conjecture" { CONJ }

| alpha*  { VAR (Lexing.lexeme lexbuf) }

| eof     { EOF }

| space+  { token lexbuf }

| retur+  { token lexbuf }

| '%' [^'\n']* '\n'
          { token lexbuf }
         
| _       {
            let message = Printf.sprintf
                          "Unknown token %s near characters %d-%d"
                          (Lexing.lexeme lexbuf)
                          (Lexing.lexeme_start lexbuf)
                          (Lexing.lexeme_end lexbuf)
            in
              raise (Lexer_Error message)
          }
