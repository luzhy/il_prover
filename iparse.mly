%{
open Syntax;;
%}

%token <string> VAR

%token AND 
%token OR 
%token NOT 
%token IMP 
%token EQU

%token AXIO
%token CONJ

%token DOT
%token COMMA
%token LPAREN
%token RPAREN 

%token EOF

%left AND OR IMP EQU
%right NOT

%start main
%type <Syntax.t_swff list> main

%%

main:
    | axiom EOF
      { [$1] }
    | conjecture EOF
      { [$1] }
    | axioms conjecture EOF
      { List.append $1 [$2] }
;

axioms:
    | axiom
      { [$1] }
    | axioms axiom
      { List.append $1 [$2] }
;

axiom:
    | VAR LPAREN VAR COMMA AXIO COMMA formula RPAREN DOT
      { Sign_T($7) }
;

conjecture:
    | VAR LPAREN VAR COMMA CONJ COMMA formula RPAREN DOT
      { Sign_F($7) }
;

formula:
    | VAR
      { Atom($1) }

    | NOT formula
      { Not($2) }

    | formula AND formula
      { And($1, $3) }

    | formula OR  formula
      { Or($1, $3) }

    | formula IMP formula
      { Imp($1, $3) }

    | formula EQU formula
      { And(Imp($1, $3), Imp($3, $1)) }

    | LPAREN formula RPAREN
      { $2 }

    | error
        {   
          let message =
            Printf.sprintf 
              "Parse error near characters %d-%d"
              (Parsing.symbol_start ()) 
              (Parsing.symbol_end ()) 
          in  
            raise (Parser_Error message)
        } 
;
