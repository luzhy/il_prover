
SRC= syntax.ml util.ml iparse.mly ilex.mll prover.ml testcase.ml main.ml
COMPONENT_TOP= syntax.ml util.ml iparse.mli iparse.ml ilex.ml prover.ml testcase.ml main.ml
TARGET= TAB_IL_Prover

all:	$(TARGET)

$(TARGET): 	$(COMPONENT_TOP)
	ocamlc $(COMPONENT_TOP) -o $(TARGET) 

iparse.mli: iparse.mly
		ocamlyacc iparse.mly

iparse.ml:  iparse.mly
		ocamlyacc iparse.mly

ilex.ml:    ilex.mll
		ocamllex ilex.mll

clean:
	/bin/rm -f $(TARGET) iparse.ml ilex.ml *.cmi *.cmo *.mli

