open Syntax;;
open Util;;
open Prover;;

let axiom_MP         () = ("MP          ",
                           [Sign_T (Atom "a");
                            Sign_T (Imp (Atom "a",
                                         Atom "b"));
                            Sign_F (Atom "b")],
                           1);;


let axiom_LEM        () = ("LEM         ",
                           [Sign_F (Or (Atom "p",
                                        Not (Atom "p")))],
                           0);;

let axiom_DNE        () = ("DNE         ",
                           [Sign_F (Imp (Not (Not (Atom "p")),
                                         Atom "p"))],
                           0);;

let axiom_THEN1      () = ("THEN-1      ",
                           [Sign_F (Imp (Atom "a",
                                         Imp (Atom "b",
                                              Atom "a")))],
                           1);;

let axiom_Pierce     () = ("Pierce-1      ",
                           [Sign_F (Imp 
                                      (Imp (Imp (Atom "p",
                                                 Atom "q"),
                                            Atom "p"),
                                       Atom "p"))],
                           0);;


let axiom_THEN2      () = ("THEN-2      ",
                           [Sign_F (Imp (Imp (Atom "a",
                                              Imp (Atom "b",
                                                   Atom "c")),
                                         Imp (Imp (Atom "a",
                                                   Atom "b"),
                                              Imp (Atom "a",
                                                   Atom "c"))))],
                           1);;

let axiom_AND1       () = ("AND-1       ",
                           [Sign_F (Imp (And (Atom "a",
                                              Atom "b"),
                                         Atom "a"))],
                           1);;

let axiom_AND2       () = ("AND-2       ",
                           [Sign_F (Imp (And (Atom "a",
                                              Atom "b"),
                                         Atom "b"))],
                           1);;

let axiom_AND3       () = ("AND-3       ",
                           [Sign_F (Imp (Atom "a",
                                         Imp (Atom "b",
                                              And (Atom "a",
                                                   Atom "b"))))],
                           1);;

let axiom_OR1        () = ("OR-1        ",
                           [Sign_F (Imp (Atom "a",
                                         Or (Atom "a",
                                             Atom "b")))],
                           1);;

let axiom_OR2        () = ("OR-2        ",
                           [Sign_F (Imp (Atom "b",
                                         Or (Atom "a",
                                             Atom "b")))],
                           1);;

let axiom_OR3        () = ("OR-3        ",
                           [Sign_F (Imp (Imp (Atom "a",
                                              Atom "b"),
                                         Imp (Imp (Atom "c",
                                                   Atom "b"),
                                              Imp (Or (Atom "a",
                                                       Atom "c"),
                                                   Atom "b"))))],
                           1);; 

let axiom_NOT1       () = ("NOT-1       ",
                           [Sign_F (Imp (Imp (Atom "a",
                                              Atom "b"),
                                         Imp (Imp (Atom "a",
                                                   Not (Atom "b")),
                                              Not (Atom "a"))))],
                           1);;

let axiom_NOT2       () = ("NOT-2       ",
                           [Sign_F (Imp (Atom "a",
                                         Imp (Not (Atom "a"),
                                              Atom "b")))],
                           1);;

let axiom_IFF1       () = ("IFF-1       ",
                           [Sign_F (Imp (And (Imp (Atom "a",
                                                   Atom "b"),
                                              Imp (Atom "b",
                                                   Atom "a")),
                                         Imp (Atom "a",
                                              Atom "b")))],
                           1);;

let axiom_IFF2       () = ("IFF-2       ",
                           [Sign_F (Imp (And (Imp (Atom "a",
                                                   Atom "b"),
                                              Imp (Atom "b",
                                                   Atom "a")),
                                         Imp (Atom "b",
                                              Atom "a")))],
                           1);;

let axiom_IFF3       () = ("IFF-3       ",
                           [Sign_F (Imp (Imp (Atom "a",
                                              Atom "b"),
                                         Imp (Imp (Atom "b",
                                                   Atom "a"),
                                              And (Imp (Atom "a",
                                                        Atom "b"),
                                                   Imp (Atom "b",
                                                        Atom "a")))))],
                           1);;

let conj_LCL181P1    () = ("LCL181+1    ", 
                           [Sign_F (And (Imp (Imp (Not (Atom "p"), 
                                                   Atom "q"), 
                                              Imp (Not (Atom "q"), 
                                                   Atom "p")), 
                                         Imp (Imp (Not (Atom "q"), 
                                                   Atom "p"), 
                                              Imp (Not (Atom "p"), 
                                                   Atom "q"))))], 
                           0);;

let conj_LCL230P1    () = ("LCL230+1    ", 
                           [Sign_F (Imp (Imp (Or (Atom "p", 
                                                  Atom "q"), 
                                              Or (Atom "p", 
                                                  Atom "r")), 
                                         Or (Atom "p", 
                                             Imp (Atom "q", 
                                                  Atom "r"))))], 
                           0);;

let conj_SYJ101P1    () = ("SYJ101+1    ", 
                           [Sign_T (Atom "a");
                            Sign_F (Atom "a")], 
                           1);;

let conj_SYJ102P1    () = ("SYJ102+1    ", 
                           [Sign_T (Atom "a");
                            Sign_F (Not (Not (Atom "a")))], 
                           1);;

let conj_SYJ103P1    () = ("SYJ103+1    ", 
                           [Sign_T (Or (Not (Atom "a"), 
                                        Not (Atom "b")));
                            Sign_F (Or (Not (Atom "b"), 
                                        Not (Atom "a")))], 
                           1);;

let conj_SYJ104P1    () = ("SYJ104+1    ", 
                           [Sign_T (Imp (Atom "a", 
                                         Atom "b")); 
                            Sign_F (Imp (Atom "a", 
                                         Atom "b"))], 
                           1);;

let conj_SYJ105P1002 () = ("SYJ105+1.002", 
                           [Sign_F (Not (Not (Or (Atom "a", 
                                                  Not (Atom "a")))))], 
                           1);;

let conj_SYJ105P1003 () = ("SYJ105+1.003", 
                           [Sign_F (Not (Not (Or (And (Atom "a", 
                                                       Atom "b"), 
                                                  Or (Not (Atom "a"), 
                                                      Not (Atom "b"))))))], 
                           1);; 

let conj_SYJ105P1004 () = ("SYJ105+1.004", 
                           [Sign_F (Not (Not (Or (And (Atom "a", 
                                                       And (Atom "b", 
                                                            Atom "c")), 
                                                  Or (Not (Atom "a"), 
                                                      Or (Not (Atom "b"), 
                                                          Not (Atom "c")))))))], 
                           1);;

let conj_SYJ106P1    () = ("SYJ106+1    ", 
                           [Sign_T (Atom "s"); Sign_T (Imp (Not (Imp (Atom "t", 
                                                                      Atom "r")), 
                                                            Atom "p")); 
                            Sign_F (Imp (Not (And (Imp (Atom "p", 
                                                        Atom "q"), 
                                                   Imp (Atom "t", 
                                                        Atom "r"))), 
                                         And (Not (Not (Atom "p")), 
                                              And (Atom "s", 
                                                   Atom "s"))))], 
                           1);;

let conj_SYJ107P1001 () = ("SYJ107+1.001", 
                           [Sign_T (Atom "c"); Sign_T (Or (Or (Atom "b", 
                                                               Atom "a"), 
                                                           Atom "b")); 
                            Sign_F (Or (Atom "a", 
                                        And (Atom "b", 
                                             Atom "c")))], 
                           1);;

let conj_SYJ107P1002 () = ("SYJ107+1.002", 
                           [Sign_T (Atom "a2");
                            Sign_T (Imp (Atom "b", 
                                         Or (Or (Atom "b1", 
                                                 Atom "a1"), 
                                             Atom "b1")));
                            Sign_T (Or (Or (Atom "b", 
                                            Atom "a"), 
                                        Atom "b"));
                            Sign_F (Or (Atom "a", 
                                        Or (And (Atom "b", 
                                                 Atom "a1"), 
                                            And (Atom "b1", 
                                                 Atom "a2"))))], 
                           1);;

let conj_SYJ201P1001 () = ("SYJ201+1.001", 
                           [Sign_T (Imp (And (Imp (Atom "p1", 
                                                   Atom "p2"), 
                                              Imp (Atom "p2", 
                                                   Atom "p1")), 
                                         And (Atom "p1", 
                                              And (Atom "p2", 
                                                   Atom "p3"))));
                            Sign_T (Imp (And (Imp (Atom "p2", 
                                                   Atom "p3"), 
                                              Imp (Atom "p3", 
                                                   Atom "p2")), 
                                         And (Atom "p1", 
                                              And (Atom "p2", 
                                                   Atom "p3"))));
                            Sign_T (Imp (And (Imp (Atom "p3", 
                                                   Atom "p1"), 
                                              Imp (Atom "p1", 
                                                   Atom "p3")), 
                                         And (Atom "p1", 
                                              And (Atom "p2", 
                                                   Atom "p3"))));
                            Sign_F (And (Atom "p1", 
                                         And (Atom "p2", 
                                              Atom "p3")))], 
                           1);;

let conj_SYJ202P1001 () = ("SYJ202+1.001", 
                           [Sign_T (Atom "o11");
                            Sign_T (Atom "o21");
                            Sign_F (Or (Atom "o11", 
                                        Atom "o21"))], 
                           1);;

let conj_SYJ203P1001 () = ("SYJ203+1.001", 
                           [Sign_T (Imp (Or (Atom "p1", 
                                             Imp (Atom "p1", 
                                                  Atom "f")), 
                                         Atom "f"));
                            Sign_F (Atom "f")], 
                           1);;

let conj_SYJ204P1001 () = ("SYJ204+1.001", 
                           [Sign_T (Atom "p1");
                            Sign_T (Imp (Atom "p1", 
                                         Imp (Atom "p1", 
                                              Atom "p0")));
                            Sign_F (Atom "p0")], 
                           1);;

let conj_SYJ205P1001 () = ("SYJ205+1.001", 
                           [Sign_F (And (Imp (And (Imp (Atom "a0", 
                                                        Atom "f"), 
                                                   And (Imp (Imp (Atom "b1", 
                                                                  Atom "b0"), 
                                                             Atom "a1"), 
                                                        Imp (Imp (Atom "b0", 
                                                                  Atom "a1"), 
                                                             Atom "a0"))), 
                                              Atom "f"), 
                                         Imp (And (Imp (Imp (Atom "b0", 
                                                             Atom "a1"), 
                                                        Atom "a0"), 
                                                   And (Imp (Imp (Atom "b1", 
                                                                  Atom "b0"), 
                                                             Atom "a1"), 
                                                        Imp (Atom "a0", 
                                                             Atom "f"))), 
                                              Atom "f")))], 
                           1);;

let conj_SYJ206P1001 () = ("SYJ206+1.001", 
                           [Sign_F (And (Imp (Atom "a1", 
                                              Atom "a1"), 
                                         Imp (Atom "a1", 
                                              Atom "a1")))], 
                           1);;

let conj_SYJ207P1001 () = ("SYJ207+1.001", 
                           [Sign_T (Imp (And (Imp (Atom "p1", 
                                                   Atom "p2"), 
                                              Imp (Atom "p2", 
                                                   Atom "p1")), 
                                         And (Atom "p1", 
                                              Atom "p2")));
                            Sign_T (Imp (And (Imp (Atom "p2", 
                                                   Atom "p1"), 
                                              Imp (Atom "p1", 
                                                   Atom "p2")), 
                                         And (Atom "p1", 
                                              Atom "p2")));
                            Sign_F (Or (Atom "p0", 
                                        Or (And (Atom "p1", 
                                                 Atom "p2"), 
                                            Not (Atom "p0"))))], 
                           0);;

let conj_SYJ208P1001 () = ("SYJ208+1.001", 
                           [Sign_T (Not (Not (Atom "o11")));
                            Sign_T (Not (Not (Atom "o21")));
                            Sign_F (Or (Atom "o11", 
                                        Atom "o21"))], 
                           0);;

let conj_SYJ209P1001 () = ("SYJ209+1.001", 
                           [Sign_T (Imp (Or (Atom "p1", 
                                             Imp (Not (Not (Atom "p1")), 
                                                  Atom "f")), 
                                         Atom "f"));
                            Sign_F (Atom "f")], 
                           0);;

let conj_SYJ210P1001 () = ("SYJ210+1.001", 
                           [Sign_T (Not (Not (Atom "p1")));
                            Sign_T (Imp (Atom "p1", 
                                         Imp (Atom "p1", 
                                              Atom "p0")));
                            Sign_F (Atom "p0")], 
                           0);;

let conj_SYJ211P1001 () = ("SYJ211+1.001", 
                           [Sign_T (Imp (Atom "a0", 
                                         Atom "f"));
                            Sign_T (Imp (Imp (Not (Not (Atom "b1")), 
                                              Atom "b0"), 
                                         Atom "a1"));
                            Sign_T (Imp (Imp (Not (Not (Atom "b0")), 
                                              Atom "a1"), 
                                         Atom "a0"));
                            Sign_F (Atom "f")], 
                           0);;

let conj_SYJ212P1001 () = ("SYJ212+1.001", 
                           [Sign_F (And (Imp (Not (Not (Atom "a1")), 
                                              Atom "a1"), 
                                         Imp (Atom "a1", 
                                              Not (Not (Atom "a1")))))], 
                           0);;


let conj_SYN001P1    () = ("SYN001+1    ", 
                           [Sign_F (And (Imp (Not (Not (Atom "p")), 
                                              Atom "p"), 
                                         Imp (Atom "p", 
                                              Not (Not (Atom "p")))))], 
                           0);;

let conj_SYN040P1    () = ("SYN040+1    ", 
                           [Sign_F (And (Imp (Imp (Atom "p", 
                                                   Atom "q"), 
                                              Imp (Not (Atom "q"), 
                                                   Not (Atom "p"))), 
                                         Imp (Imp (Not (Atom "q"), 
                                                   Not (Atom "p")), 
                                              Imp (Atom "p", 
                                                   Atom "q"))))], 
                           0);;

let conj_SYN041P1    () = ("SYN041+1    ", 
                           [Sign_F (Imp (Not (Imp (Atom "p", 
                                                   Atom "q")), 
                                         Imp (Atom "q", 
                                              Atom "p")))], 
                           1);;

let conj_SYN044P1    () = ("SYN044+1    ", 
                           [Sign_T (Imp (Atom "q", 
                                         Atom "r"));
                            Sign_T (Imp (Atom "r", 
                                         And (Atom "p", 
                                              Atom "q")));
                            Sign_T (Imp (Atom "p", 
                                         Or (Atom "q", 
                                             Atom "r")));
                            Sign_F (And (Imp (Atom "p", 
                                              Atom "q"), 
                                         Imp (Atom "q", 
                                              Atom "p")))], 
                           1);;

let conj_SYN045P1    () = ("SYN045+1    ", 
                           [Sign_F (And (Imp (Or (Atom "p", 
                                                  And (Atom "q", 
                                                       Atom "r")), 
                                              And (Or (Atom "p", 
                                                       Atom "q"), 
                                                   Or (Atom "p", 
                                                       Atom "r"))), 
                                         Imp (And (Or (Atom "p", 
                                                       Atom "q"), 
                                                   Or (Atom "p", 
                                                       Atom "r")), 
                                              Or (Atom "p", 
                                                  And (Atom "q", 
                                                       Atom "r")))))], 
                           1);;

let conj_SYN046P1    () = ("SYN046+1    ", 
                           [Sign_F (And (Imp (Imp (Atom "p", 
                                                   Atom "q"), 
                                              Or (Not (Atom "p"), 
                                                  Atom "q")), 
                                         Imp (Or (Not (Atom "p"), 
                                                  Atom "q"), 
                                              Imp (Atom "p", 
                                                   Atom "q"))))], 
                           0);;

let conj_SYN047P1    () = ("SYN047+1    ", 
                           [Sign_F (And (Imp (Imp (And (Atom "p", 
                                                        Imp (Atom "q", 
                                                             Atom "r")), 
                                                   Atom "s"), 
                                              And (Or (Or (Not (Atom "p"), 
                                                           Atom "q"), 
                                                       Atom "s"), 
                                                   Or (Or (Not (Atom "p"), 
                                                           Not (Atom "r")), 
                                                       Atom "s"))), 
                                         Imp (And (Or (Or (Not (Atom "p"), 
                                                           Atom "q"), 
                                                       Atom "s"), 
                                                   Or (Or (Not (Atom "p"), 
                                                           Not (Atom "r")), 
                                                       Atom "s")), 
                                              Imp (And (Atom "p", 
                                                        Imp (Atom "q", 
                                                             Atom "r")), 
                                                   Atom "s"))))], 
                           0);;

let conj_SYN387P1    () = ("SYN387+1    ", 
                           [Sign_F (Or (Atom "p", 
                                        Not (Atom "p")))], 
                           0);;

let conj_SYN388P1    () = ("SYN388+1    ", 
                           [Sign_F (Or (Atom "p", 
                                        Not (Not (Not (Atom "p")))))] , 
                           0);;

let conj_SYN389P1    () = ("SYN389+1    ", 
                           [Sign_F (Imp (Imp (Imp (Atom "p", 
                                                   Atom "q"), 
                                              Atom "p"), 
                                         Atom "p"))], 
                           0);;

let conj_SYN390P1    () = ("SYN390+1    ", 
                           [Sign_F (And (Imp (Atom "p", 
                                              Atom "p"), 
                                         Imp (Atom "p", 
                                              Atom "p")))] , 
                           1);;

let conj_SYN391P1    () = ("SYN391+1    ", 
                           [Sign_F (Imp (And (And (Or (Atom "p1", 
                                                       Atom "p2"), 
                                                   Or (Not (Atom "p1"), 
                                                       Atom "p2")), 
                                              Or (Atom "p1", 
                                                  Not (Atom "p2"))), 
                                         Not (Or (Not (Atom "p1"), 
                                                  Not (Atom "p2")))))] , 
                           1);;

let conj_SYN392P1    () = ("SYN392+1    ", 
                           [Sign_F (And (Imp (And (Imp (Atom "p1", 
                                                        Atom "p2"), 
                                                   Imp (Atom "p2", 
                                                        Atom "p1")), 
                                              And (Or (Atom "p2", 
                                                       Not (Atom "p1")), 
                                                   Or (Not (Atom "p2"), 
                                                       Atom "p1"))), 
                                         Imp (And (Or (Atom "p2", 
                                                       Not (Atom "p1")), 
                                                   Or (Not (Atom "p2"), 
                                                       Atom "p1")), 
                                              And (Imp (Atom "p1", 
                                                        Atom "p2"), 
                                                   Imp (Atom "p2", 
                                                        Atom "p1")))))], 
                           0);;

let conj_SYN393P1    () = ("SYN393+1    ", 
                           [Sign_F (And (Imp (And (Imp (And (Imp (Atom "p1", 
                                                                  Atom "p2"), 
                                                             Imp (Atom "p2", 
                                                                  Atom "p1")), 
                                                        Atom "p3"), 
                                                   Imp (Atom "p3", 
                                                        And (Imp (Atom "p1", 
                                                                  Atom "p2"), 
                                                             Imp (Atom "p2", 
                                                                  Atom "p1")))), 
                                              And (Imp (Atom "p1", 
                                                        And (Imp (Atom "p2", 
                                                                  Atom "p3"), 
                                                             Imp (Atom "p3", 
                                                                  Atom "p2"))), 
                                                   Imp (And (Imp (Atom "p2", 
                                                                  Atom "p3"), 
                                                             Imp (Atom "p3", 
                                                                  Atom "p2")), 
                                                        Atom "p1"))), 
                                         Imp (And (Imp (Atom "p1", 
                                                        And (Imp (Atom "p2", 
                                                                  Atom "p3"), 
                                                             Imp (Atom "p3", 
                                                                  Atom "p2"))), 
                                                   Imp (And (Imp (Atom "p2", 
                                                                  Atom "p3"), 
                                                             Imp (Atom "p3", 
                                                                  Atom "p2")), 
                                                        Atom "p1")), 
                                              And (Imp (And (Imp (Atom "p1", 
                                                                  Atom "p2"), 
                                                             Imp (Atom "p2", 
                                                                  Atom "p1")), 
                                                        Atom "p3"), 
                                                   Imp (Atom "p3", 
                                                        And (Imp (Atom "p1", 
                                                                  Atom "p2"), 
                                                             Imp (Atom "p2", 
                                                                  Atom "p1")))))))], 
                           0);;

let conj_SYN416P1    () = ("SYN416+1    ", 
                           [Sign_F (Or (Imp (Atom "p",
                                             Atom "q"),
                                        Or (Imp (Atom "q", 
                                                 Atom "r"), 
                                            Imp (Atom "r", 
                                                 Atom "p"))))], 
                           0);;

let conj_SYN977P1    () = ("SYN977+1    ", 
                           [Sign_F (Or (Or (And (Imp (Atom "a", 
                                                      Atom "b"), 
                                                 Imp (Atom "b", 
                                                      Atom "a")), 
                                            Atom "a"), 
                                        Atom "b"))], 
                           0);;

let conj_SYN978P1    () = ("SYN978+1    ", 
                           [Sign_F (Imp (And (Atom "a", 
                                              Atom "b"), 
                                         And (Imp (Atom "a", 
                                                   Atom "b"), 
                                              Imp (Atom "b", 
                                                   Atom "a"))))], 
                           1);;

let case_list () = [
  axiom_MP;
  axiom_LEM;
  axiom_DNE;
  axiom_THEN1;
  axiom_THEN2;
  axiom_AND1;
  axiom_AND2;
  axiom_AND3;
  axiom_OR1;
  axiom_OR2;
  axiom_OR3;
  axiom_NOT1;
  axiom_NOT2;
  axiom_IFF1;
  axiom_IFF2;
  axiom_IFF3;
  conj_LCL181P1;
  conj_LCL230P1;
  conj_SYJ101P1;
  conj_SYJ102P1;
  conj_SYJ103P1;
  conj_SYJ104P1;
  conj_SYJ105P1002;
  conj_SYJ105P1003;
  conj_SYJ105P1004;
  conj_SYJ106P1;
  conj_SYJ107P1001;
  conj_SYJ107P1002;
  conj_SYJ201P1001;
  conj_SYJ202P1001;
  conj_SYJ203P1001;
  conj_SYJ204P1001;
  conj_SYJ205P1001;
  conj_SYJ206P1001;
  conj_SYJ207P1001;
  conj_SYJ208P1001;
  conj_SYJ209P1001;
  conj_SYJ210P1001;
  conj_SYJ211P1001;
  conj_SYJ212P1001;
  conj_SYN001P1;
  conj_SYN040P1;
  conj_SYN041P1;
  conj_SYN044P1;
  conj_SYN045P1;
  conj_SYN046P1;
  conj_SYN047P1;
  conj_SYN387P1;
  conj_SYN388P1;
  conj_SYN389P1;
  conj_SYN390P1;
  conj_SYN391P1;
  conj_SYN392P1;
  conj_SYN393P1;
  conj_SYN416P1;
  conj_SYN977P1;
  conj_SYN978P1
];;

let choose_name case =
  match case with
    | (f1, f2, f3) -> f1
;;

let choose_data case =
  match case with
    | (f1, f2, f3) -> f2
;;

let choose_resu case =
  match case with
    | (f1, f2, f3) -> f3
;;

let check_case case =
  let tup = case () in
    print_string "Case Name    : "; print_endline (choose_name tup);
    print_string "Formula Sets : "; print_set (choose_data tup); print_endline ""; 
    print_string "Provable     : "; if (choose_resu tup) = 1 then print_string "True" else print_string "False";
;;

let test_one case =
  let tup = case () in
    print_string (choose_name tup);
    if (choose_resu tup) = 1 then print_string " -- T" else print_string " --NT";
    print_string " | result: ";
    begin
      match prove (choose_data tup) with
        | Real_Proof x -> 
            print_endline " T"; 
            print_endline "Proof Table: "; 
            print_prooftable x
        | Null_Proof x -> 
            print_endline "NT"; 
            print_endline "Counter Model: "; 
            print_worlds  x
    end
;;

let test_all () =
  let rec iter ilist =
    match ilist with
      | []     -> ()
      | hd::tl ->
          let tup = hd () in
            print_string (choose_name tup);
            if (choose_resu tup) = 1 then print_string " -- T" else print_string " --NT";
            print_string " | result: ";
            begin
              match prove (choose_data tup) with
                | Real_Proof x -> print_endline " T" 
                | Null_Proof x -> print_endline "NT"
            end;
            iter tl
  in
    iter (case_list ())
;;

let list_all () = 
  "axiom_MP; axiom_LEM; axiom_DNE; axiom_THEN1; axiom_THEN2; axiom_AND1; axiom_AND2; axiom_AND3; axiom_OR1; axiom_OR2; axiom_OR3; axiom_NOT1; axiom_NOT2; axiom_IFF1; axiom_IFF2; axiom_IFF3; conj_LCL181P1; conj_LCL230P1; conj_SYJ101P1; conj_SYJ102P1; conj_SYJ103P1; conj_SYJ104P1; conj_SYJ105P1002; conj_SYJ105P1003; conj_SYJ105P1004; conj_SYJ106P1; conj_SYJ107P1001; conj_SYJ107P1002; conj_SYJ201P1001; conj_SYJ202P1001; conj_SYJ203P1001; conj_SYJ204P1001; conj_SYJ205P1001; conj_SYJ206P1001; conj_SYJ207P1001; conj_SYJ208P1001; conj_SYJ209P1001; conj_SYJ210P1001; conj_SYJ211P1001; conj_SYJ212P1001; conj_SYN001P1; conj_SYN040P1; conj_SYN041P1; conj_SYN044P1; conj_SYN045P1; conj_SYN046P1; conj_SYN047P1; conj_SYN387P1; conj_SYN388P1; conj_SYN389P1; conj_SYN390P1; conj_SYN391P1; conj_SYN392P1; conj_SYN393P1; conj_SYN416P1; conj_SYN977P1; conj_SYN978P1"
;;
